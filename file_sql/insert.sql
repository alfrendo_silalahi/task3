create table karyawan(
    id varchar(20) not null ,
    nama varchar(50) not null ,
    jenis_kelamin varchar(1) not null ,
    status varchar(30) not null ,
    tanggal_lahir date not null ,
    tanggal_masuk date not null ,
    departemen varchar(20) not null
);

create table departemen(
    id varchar(20) not null ,
    nama varchar(30) not null
);

insert into departemen values
    ('1', 'Manajemen'),
    ('2', 'Pengembangan Bisnis'),
    ('3', 'Teknisi'),
    ('4', 'Analisis');

insert into karyawan values
('1', 'Rizki Saputra', 'L', 'Menikah', str_to_date('10-11-1980', '%m-%d-%Y'), str_to_date('01-01-2011', '%m-%d-%Y'), '1'),
('2', 'Farhan Reza', 'L', 'Belum', str_to_date('11-01-1989', '%m-%d-%Y'), str_to_date('01-01-2011', '%m-%d-%Y'), '1'),
('3', 'Riyando Adi', 'L', 'Menikah', str_to_date('01-25-1977', '%m-%d-%Y'), str_to_date('01-01-2011', '%m-%d-%Y'), '1'),
('4', 'Diego Manuel', 'L', 'Menikah', str_to_date('02-22-1983', '%m-%d-%Y'), str_to_date('09-04-2012', '%m-%d-%Y'), '2'),
('5', 'Satya Laksana', 'L', 'Menikah', str_to_date('01-12-1981', '%m-%d-%Y'), str_to_date('03-19-2011', '%m-%d-%Y'), '2'),
('6', 'Miguel Hernandez', 'L', 'Menikah', str_to_date('10-16-1994', '%m-%d-%Y'), str_to_date('06-15-2014', '%m-%d-%Y'), '2'),
('7', 'Putri Persada', 'P', 'Menikah', str_to_date('01-30-1988', '%m-%d-%Y'), str_to_date('04-14-2013', '%m-%d-%Y'), '2'),
('8', 'Alma Safira', 'P', 'Menikah', str_to_date('05-18-1991', '%m-%d-%Y'), str_to_date('09-28-2013', '%m-%d-%Y'), '3'),
('9', 'Haqi Hafiz', 'L', 'Belum', str_to_date('09-19-1995', '%m-%d-%Y'), str_to_date('03-09-2015', '%m-%d-%Y'), '3'),
('10', 'Abi Isyawara', 'L', 'Belum', str_to_date('06-03-1991', '%m-%d-%Y'), str_to_date('01-22-2012', '%m-%d-%Y'), '3'),
('11', 'Maman Kresna', 'L', 'Belum', str_to_date('08-21-1993', '%m-%d-%Y'), str_to_date('09-15-2012', '%m-%d-%Y'), '3'),
('12', 'Nadia Aulia', 'P', 'Belum', str_to_date('10-07-1989', '%m-%d-%Y'), str_to_date('05-07-2012', '%m-%d-%Y'), '4'),
('13', 'Mutiara Rezki', 'P', 'Menikah', str_to_date('03-23-1988', '%m-%d-%Y'), str_to_date('05-21-2013', '%m-%d-%Y'), '4'),
('14', 'Dani Setiawan', 'L', 'Belum', str_to_date('02-11-1986', '%m-%d-%Y'), str_to_date('11-30-2014', '%m-%d-%Y'), '4'),
('15', 'Budi Putra', 'L', 'Belum', str_to_date('10-23-1995', '%m-%d-%Y'), str_to_date('12-03-2015', '%m-%d-%Y'), '4');

